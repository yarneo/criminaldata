//
//  ViewController.swift
//  CriminalData
//
//  Created by Yarden Eitan on 1/29/16.
//  Copyright © 2016 Yarden Eitan. All rights reserved.
//

import UIKit
import MapKit

extension UIColor {
    
    convenience init(hex: Int) {
        
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
        
    }
    
}

class MapViewController : UIViewController, MKMapViewDelegate {
    
    var showPins = false
    
    let METERS_PER_MILE = 1609.344
    let colors = [0xff0000, 0xeb3600, 0xe54800, 0xd86d00, 0xd27f00, 0xc5a300, 0xb9c800, 0xa6ff00, 0xa6ff00, 0xa6ff00]
    
    private var myContext = 0
    var criminalData : CriminalData?
    let spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    var annotations = [MKPointAnnotation]()
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let locationCoordinate = CLLocationCoordinate2D(latitude: 37.7545565620279, longitude: -122.419711251166)
        let viewRegion = MKCoordinateRegionMakeWithDistance(locationCoordinate, METERS_PER_MILE*15, METERS_PER_MILE*15)
        
        self.mapView.setRegion(viewRegion, animated: true)
        self.mapView.delegate = self
        
        self.criminalData = CriminalData()
        self.criminalData?.addObserver(self, forKeyPath: "mostDangerousDistricts", options: .New, context:&myContext)
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 20))
        button.setTitle("Show Pins", forState: UIControlState.Normal)
        button.addTarget(self, action: "pinsAction:", forControlEvents: UIControlEvents.TouchUpInside)
        let leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        let rightBarButtonItem = UIBarButtonItem(customView: spinner)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        self.spinner.startAnimating()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pinsAction(sender : UIButton) {
        if showPins {
            mapView.removeAnnotations(annotations)
            
            sender.setTitle("Show Pins", forState: UIControlState.Normal)
            showPins = false
        } else {
            mapView.addAnnotations(annotations)
            
            sender.setTitle("Hide Pins", forState: UIControlState.Normal)
            showPins = true
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &myContext {
            if let newValue = change?[NSKeyValueChangeNewKey] {
                print("Array changed: \(newValue)")
                
                self.spinner.stopAnimating()
                
                var i = 0
                for (district,dataPoints) in criminalData!.criminalPointsByDistricts {
                    print("\(district) has \(dataPoints.count) crime count")
                    addBoundry(dataPoints, district: district)
                    i++
                }
                
            }
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
        
    }
    
    
    func addBoundry(dangerPoints : [DangerPoint], district : String)
    {
        let points = dangerPoints.map({
            $0.coordinates!
        })
        
        for point in points {
            let newAnotation = MKPointAnnotation()
            newAnotation.coordinate = point
            self.annotations.append(newAnotation)
        }
        
        let giftWrappingAlgo = GiftWrappingAlgorithm(points: points)
        var newPolygonPoints = giftWrappingAlgo.getPolygon()
        let polygon = MKPolygon(coordinates: &newPolygonPoints, count: newPolygonPoints.count)
        polygon.title = district
        mapView.addOverlay(polygon)
    }
    
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolygon {
            let polygonView = MKPolygonRenderer(overlay: overlay)
            let dangerousDistrictRank = self.criminalData?.mostDangerousDistricts.indexOf(overlay.title!!)
            if let ddr = dangerousDistrictRank {
                polygonView.fillColor = UIColor(hex:self.colors[ddr]).colorWithAlphaComponent(0.2)
            }
            return polygonView
        }
        
        return MKPolylineRenderer()
    }
    
    
    deinit {
        self.criminalData?.removeObserver(self, forKeyPath: "mostDangerousDistricts", context: &myContext)
    }
      
    
}

