//
//  CriminalData.swift
//  CriminalData
//
//  Created by Yarden Eitan on 1/29/16.
//  Copyright © 2016 Yarden Eitan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import MapKit

class DangerPoint {
    var district : String?
    var coordinates : CLLocationCoordinate2D?
    var address : String?
    var category : String?
    var date : NSDate?
    
    init(jsonInput : [String:JSON]?) {
        self.district = jsonInput?["pddistrict"]?.string
        if let x = jsonInput?["x"]?.string, y = jsonInput?["y"]?.string {
            self.coordinates = CLLocationCoordinate2D(latitude: Double(y)!, longitude: Double(x)!)
        }
        self.address = jsonInput?["address"]?.string
        self.category = jsonInput?["category"]?.string
        
        if let d = jsonInput?["date"]?.string {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            self.date = formatter.dateFromString(d)
        }
    }
    
}


class CriminalData : NSObject {
    
    var criminalPointsByDistricts = [String:[DangerPoint]]()
    dynamic var mostDangerousDistricts = [String]()
    
    override init() {
        super.init()
        
        self.fetchCriminalData()
        
    }
    
    func fetchCriminalData() {
        
        guard let encodedURL = self.getEncodedURL() else {
            return
        }

        Alamofire.request(.GET, encodedURL, parameters: nil)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .Success(let result):
                    let json = JSON(result)
                    
                    if let pointsArr = json.array {
                        for point in pointsArr {
                            let dp = DangerPoint(jsonInput: point.dictionary)
                            
                            //count how many criminal activies exist in every district
                            if let district = dp.district {
                                if self.criminalPointsByDistricts[district] != nil {
                                    self.criminalPointsByDistricts[district]!.append(dp)
                                } else {
                                    self.criminalPointsByDistricts[district] = [dp]
                                }
                            }
                            
                        }
                    }
                    
                    self.mostDangerousDistricts = self.parseByDistricts()
                    
                    
                case .Failure(let error):
                    print(error)
                    
                    
                }

        }
        
    }
    
    func getEncodedURL() -> String? {
        let date = NSDate()
        let dateComponents = NSDateComponents()
        //Did a fetch for last 2 months, as 1 month returned no data
        dateComponents.day -= 60
        let monthAgoDate = NSCalendar.currentCalendar().dateByAddingComponents(dateComponents, toDate: date, options: [])
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let monthAgo = formatter.stringFromDate(monthAgoDate!)
        
        let resourceString = "https://data.sfgov.org/resource/ritf-b9ki.json?$where=date>'\(monthAgo)'"
        let encodedURL = resourceString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        
        return encodedURL
    }
    
    func parseByDistricts() -> [String] {
        let myArr = Array(self.criminalPointsByDistricts.keys)
        let sortedKeys = myArr.sort() {
            return self.criminalPointsByDistricts[$0]!.count > self.criminalPointsByDistricts[$1]!.count
        }
        return sortedKeys
    }
    
}