//
//  GiftWrappingAlgorithm.swift
//  CriminalData
//
//  Created by Yarden Eitan on 1/30/16.
//  Copyright © 2016 Yarden Eitan. All rights reserved.
//

import Foundation
import MapKit

class GiftWrappingAlgorithm {

    enum Direction : Int {
        case TURN_LEFT = 1
        case TURN_RIGHT = -1
        case TURN_NONE = 0
    }
    
    var points : [CLLocationCoordinate2D]!
    
    init(points : [CLLocationCoordinate2D]) {
        self.points = points
    }
    
    func leftmostPoint() -> CLLocationCoordinate2D {
        var leftmostPoint = points[0]
        var leftmostX = leftmostPoint.latitude
        for point in points {
            if point.latitude > leftmostX {
                leftmostX = point.latitude
                leftmostPoint = point
            }
        }
        
        return leftmostPoint
    }

    func turn(p : CLLocationCoordinate2D, q : CLLocationCoordinate2D, r : CLLocationCoordinate2D) -> Direction {
        let calc = (q.latitude - p.latitude)*(r.longitude - p.longitude) - (r.latitude - p.latitude)*(q.longitude - p.longitude)
        if calc < 0 {
            return Direction.TURN_RIGHT
        } else if calc == 0 {
            return Direction.TURN_NONE
        } else {
            return Direction.TURN_LEFT
        }
    }
    
    //Returns the squared Euclidean distance between point1 and point2.
    func distance(point1 : CLLocationCoordinate2D, point2 : CLLocationCoordinate2D) -> Double {
        let (dx, dy) = (point2.latitude - point1.latitude, point2.longitude - point1.longitude)
        return dx * dx + dy * dy
    }
    
    //Returns the next point on the convex hull in counter clockwise from point.
    func findNextHullPoint(point : CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        var nextPoint = point
        for p in self.points {
            let t = turn(point, q: nextPoint, r: p)
            if t == Direction.TURN_RIGHT || t == Direction.TURN_NONE && distance(point, point2: p) > distance(point, point2: nextPoint) {
                nextPoint = p
            }
        }
        return nextPoint
    }
    
    //Returns the points on the convex hull of points in counter clockwise order.
    func jarvisMarch(initialPoint : CLLocationCoordinate2D) -> [CLLocationCoordinate2D] {
        var hull = [initialPoint]
        var currPoint = initialPoint
        while true {
            let nextPoint = findNextHullPoint(currPoint)
            if nextPoint.latitude != initialPoint.latitude || nextPoint.longitude != initialPoint.longitude {
                hull.append(nextPoint)
            } else {
                break
            }
            currPoint = nextPoint
        }
        return hull
    }
    
    func getPolygon() -> [CLLocationCoordinate2D] {
        return jarvisMarch(leftmostPoint())
    }

}